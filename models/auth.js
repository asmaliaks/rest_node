var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/libs/mongoose');
var uuid = require("uuid");
var Schema = mongoose.Schema;

var schema = new Schema({
    user_id: {
        type: String,
        unique: true,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    }
});

schema.statics.createToken = function(userId, callback){
    var Auth = this;
    if(!userId){
        var error = {
            status: 400,
            message: "Please provide following parameters : userId"
        };
        callback(error);
    }else{
        var token = uuid.v4();

        var auth = new Auth({
            user_id: userId,
            token: token
        });

        auth.save(function(err){
            if(err){
                var error = {
                    status: 400,
                    message: err.message
                };
                callback(error);
            }else{
                callback(null, auth);
            }
        });
    }
};

schema.statics.checkToken = function(token, callback){
    var Auth = this;
    if(!token){
        var error = {
            status: 400,
            message: "Please provide following parameters : token"
        };
        callback(error);
    }else{
        Auth.findOne({token: token}, function(err, auth){ console.log(token);
            if(err){
                var error = {
                    status: 500,
                    message: err.message
                };
                callback(error);
            }else{
                if(!auth){
                    var error = {
                        status: 403,
                        message: 'The token is not valid'
                    };
                    callback(error);
                }else{
                    callback(null, auth);
                }
            }
        })
    }
};
exports.Auth = mongoose.model('Auth', schema);
