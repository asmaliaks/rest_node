var appRoot = require('app-root-path');
var crypto = require('crypto');
var async = require('async');
var mongoose = require(appRoot + '/libs/mongoose');
var Auth = require(appRoot+'/models/auth').Auth;
var Schema = mongoose.Schema;

var schema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.methods.encryptPassword = function(password){
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
}

schema.virtual('password')
    .set(function(password){
        this._plainPassword = password;
        this.salt = Math.random()+'';
        this.hashedPassword =  this.encryptPassword(password);
    })
    .get(function(){
        return this._plainPassword;
    });

schema.methods.checkPassword = function(password){
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function(username, password, callback){

    if(!username || !password) {
        var errorString = '';
        if (!username) errorString = errorString + ' username ';
        if (!password) errorString = errorString + ' password ';
        var message = 'Please provide following params :' + errorString;
        var err = {
            status: 400,
            message: message
        }
        callback(err);
    }else{
        // check if username exists and password matches
        var User = this;
        async.waterfall([
                function(callback){
                    User.findOne({username: username}, callback);
                },
                function(user, callback){
                    if(user){
                        if(user.checkPassword(password)){
                            Auth.findOne({user_id: user._id}, function(err, auth){
                                if(err){
                                    callback(err);
                                }else{
                                    callback(null, user, auth['token']);
                                }
                            });
                        }else{
                            var error = {
                                status: 400,
                                message: 'Wrong password'
                            };
                            callback(error);
                        }
                    }
                }],
            callback
        );
    }
};

schema.statics.register = function(data, callback){

    if(!data.username || !data.email || !data.password){
        var errorString = '';
        if(!data.username) errorString = errorString + ' username ';
        if(!data.email) errorString = errorString + ' email ';
        if(!data.password) errorString = errorString + ' password ';
        var message = 'Please provide following params :' + errorString;
        var err = {
            status: 400,
            message: message
        }
        callback(err);
    }else{
        var User = this;

        var user = new User({
            username: data.username,
            email: data.email,
            password: data.password
        });
        if(!data.role){
            user.role = 'user';
        }else{
            user.role = data.role;
        }
        user.save(function(err){
            if(err){
                var error = {
                    status: 400,
                    message: err.message
                };
                callback(error);
            }else{
                callback(null, user);
            }

        });
    }

};

schema.statics.checkUserAuth = function(auth, callback){
    var userId = auth.user_id;
    var User = this;

    User.findById(userId, function(err, user){
        if(err){
            var error = {
                status: 500,
                message: err.message
            };
            callback(error);
        }else{
            if(!user){
                var error = {
                    status: 403,
                    message: "User with id = ["+userId+"] not found."
                };
                callback(error);
            }else{
                callback(null, user);
            }
        }

    });
};

exports.User = mongoose.model('User', schema);