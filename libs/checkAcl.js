var appRoot = require('app-root-path');
var log = require(appRoot+'/libs/log.js')(module);

exports.admin = function(req, res, next){
    if(req.user.role != 'admin'){
        res.statusCode = 403;
        return res.send({error: "Forbidden access"});
    }else{
        next();
    }
};

exports.user = function(req, res, next){
    if(req.user.role != 'user'){
        res.statusCode = 403;
        return res.send({error: "Forbidden access"});
    }else{
        next();
    }
};

