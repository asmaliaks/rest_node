var appRoot = require('app-root-path');
var Auth = require(appRoot+'/models/auth').Auth;
var User = require(appRoot+'/models/user').User;
var log = require(appRoot+'/libs/log.js')(module);

function checkAuth(req, res, next){
    var token  = req.headers.authorization;
    // check if token exists
    Auth.checkToken(token, function(err, auth){
        if(err){
            res.statusCode = err.status;
            log.error('Error(%d): %s', res.statusCode, err.message);
            return res.send({error: err.message});
        }else{
            if(!auth){
                res.statusCode = 403;
                return res.send({error: 'Forbidden access'});
            }else{
                User.checkUserAuth(auth, function(err, user){
                    if(err){
                        res.statusCode = err.status;
                        log.error('Internal error(%d): %s', res.statusCode, err.message);
                        return res.send({error: err.message});
                    }else{
                        req.user = user;
                        next();
                    }
                })
            }
        }
    })
}

module.exports = checkAuth;