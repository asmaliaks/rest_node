var appRoot = require('app-root-path');
var User = require(appRoot + '/models/user').User;
var log = require(appRoot+'/libs/log.js')(module);
var AuthError = require(appRoot + '/models/user').AuthError;

exports.post = function(req, res, next){

    if(!req.body.username || req.body.username == '' || !req.body.password || req.body.password == ''){
        var errorMessage = "Please provide username or/and password";
        res.statusCode = 400;
        return res.send({error: errorMessage});
    }else{
        var username = req.body.username;
        var password = req.body.password;
        User.authorize(username, password, function(err, user, token){
            // handle error if it exsists
            if(err){
                res.statusCode = err.status;
                return res.send({error: err.message});
            }else{
                // if all good send user and token
                res.stausCode = 200;
                return res.send({user: user, token: token });
            }
        });
    }
};