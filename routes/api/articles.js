var appRoot = require('app-root-path');
var log = require(appRoot+'/libs/log.js')(module);
var ArticleModel = require(appRoot+'/models/articles').ArticleModel;
var fs = require('fs');
var uuid = require("uuid");

exports.get = function(req, res){

    if(!req.query.id){
        return ArticleModel.find(function (err, articles) {
            if (!err) {
                return res.send(articles);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({error: 'Server error'});
            }
        });
    }else{
        return ArticleModel.findById(req.query.id, function (err, article) {
            if(!article) {
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            }
            if (!err) {
                return res.send({ status: 'OK', article:article });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
};

exports.post = function(req, res){
    if(!req.body.id) {
        var article = new ArticleModel({
            title: req.body.title,
            author: req.body.author,
            description: req.body.description,
            image: null
        });

        article.save(function (err) {
            if (!err) {

                if(req.files.image){

                    console.log("Uploading: " + req.files.image.name);
                    fs.readFile(req.files.image.path, function (err, data) {
                        var fileId = uuid.v4();
                        var newName = fileId + req.files.image.name;
                        var newPath = appRoot.path+ '/public/images/articles/' + newName;
                        fs.writeFile(newPath, data, function (err) {
                            if (err) throw err;

                            ArticleModel.findById(article.id, function (err, articleImage) {
                                if(!articleImage) {
                                    res.statusCode = 404;
                                    return res.send({ error: 'Not found' });
                                }

                                articleImage.image = newName;
                                articleImage.save(function(err){
                                    if (!err) {
                                        log.info("article updated");
                                        return res.send({ status: 'OK', article:articleImage });
                                    } else {
                                        if(err.name == 'ValidationError') {
                                            res.statusCode = 400;
                                            res.send({ error: 'Validation error' });
                                        } else {
                                            res.statusCode = 500;
                                            res.send({ error: 'Server error' });
                                        }
                                        log.error('Internal error(%d): %s',res.statusCode,err.message);return;
                                    }
                                });
                            });
                        });
                    });
                }else{
                    log.info("article created");
                    return res.send({status: 'OK', article: article});
                }

            } else {

                if (err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({error: 'Validation error'});
                } else {
                    res.statusCode = 500;
                    res.send({error: 'Server error'});
                }
                log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
        });
    }else{
        return ArticleModel.findById(req.body.id, function (err, article) {
            if (!article) {
                res.statusCode = 404;
                return res.send({error: 'Not found'});
            }

            article.title = req.body.title;
            article.description = req.body.description;
            article.author = req.body.author;

            if (req.files.image) {
                // remove old image from directory
                fs.unlink(appRoot.path + '/public/images/articles/' + article.image, function (err) {
                    if (err) {
                        log.info("Unlink error");
                        return res.send({status: 500, err: err});
                    }

                    console.log('Old image successfully removed');
                })
                //  change image
                var fileId = uuid.v4();
                var newName = fileId + req.files.image.name;
                var newPath = appRoot.path + '/public/images/articles/' + newName;
                fs.writeFile(newPath, function (err) {
                    if (err) throw err;

                    article.image = newName;
                    return article.save(function (err) {
                        if (!err) {
                            log.info("article updated");
                            return res.send({status: 'OK', article: article});
                        } else {
                            log.error('Internal error(%d): %s', res.statusCode, err.message);
                            if (err.name == 'ValidationError') {
                                res.statusCode = 400;
                                return res.send({error: 'Validation error'});
                            } else {
                                res.statusCode = 500;
                                return res.send({error: 'Server error'});
                            }

                        }
                    });
                });
            }else{

                return article.save(function (err) {
                    if (!err) {
                        log.info("article updated");
                        return res.send({status: 'OK', article: article});
                    } else {
                        if (err.name == 'ValidationError') {
                            res.statusCode = 400;
                            res.send({error: 'Validation error'});
                        } else {
                            res.statusCode = 500;
                            res.send({error: 'Server error'});
                        }
                        log.error('Internal error(%d): %s', res.statusCode, err.message);
                    }
                });
            }
        });
    }
};

exports.put = function(req, res){
    res.status(200).send('api/articles PUT not emplemented');
};

exports.delete = function(req, res){
    if(req.query.id) {
        return ArticleModel.findById(req.query.id, function (err, article) {
            if (!article) {
                log.info("Article with id = "+req.query.id+' not found.');
                res.statusCode = 404;
                return res.send({error: 'Not found'});
            }
            var article = article;
            return article.remove(function (err) {
                if (!err) {
                    if (article.image != null) {
                        fs.unlink(appRoot.path + '/public/images/articles/' + article.image, function (err) {
                            if (err) {
                                log.info("Aricle removed. Unlink error.");
                                return res.status(200).send({id: article._id});
                            } else {
                                log.info("Article and image removed");
                                return res.status(200).send({id: article._id});
                            }
                        });
                    } else {
                        log.info("article removed");
                        return res.status(200).send({id: article._id});
                    }
                } else {
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s', res.statusCode, err.message);
                    return res.send({error: 'Server error'});
                }
            });
        });
    }else{
        log.info("No one has priviliges to remove all articles. Please provide variable [id].");
        return res.status(403).send({error: 'Permission error'});
    }
};