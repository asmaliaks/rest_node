var appRoot = require('app-root-path');
var User = require(appRoot+'/models/user').User;
var Auth = require(appRoot+'/models/auth').Auth;
var log = require(appRoot+'/libs/log.js')(module);

exports.post = function(req, res, next){

    var data = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    };

    if(req.body.role) data.role = req.body.role;

    User.register(data, function(err, user){
        if(err){
            log.error('Internal error(%d): %s', err.status, err.message);
            res.statusCode = err.status;
            return res.send({error: err.message});
        }else{
            console.log(user);
            Auth.createToken(user._id, function(err, auth){
                if(err){
                    log.error('Internal error(%d): %s', err.status, err.message);
                    res.statusCode = err.status;
                    return res.send({error: err.message});
                }else{

                    res.statusCode = 200;
                    return res.send({ status: 'OK', token: auth.token});
                }
            });

        }

    });

};