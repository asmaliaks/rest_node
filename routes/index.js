var appRoot = require('app-root-path');
var log = require(appRoot+'/libs/log.js')(module);
var checkAcl = require(appRoot+'/libs/checkAcl.js');
var checkAuth = require(appRoot+'/libs/checkAuth.js');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function(app){
    app.post('/api/login', multipartMiddleware, require('./api/login').post);

    app.post('/api/articles', checkAuth, checkAcl.admin, requiremultipartMiddleware, require('./api/articles').post);
    app.post('/api/articles/:id', checkAuth, checkAcl.admin, multipartMiddleware, require('./api/articles').post);
    app.get('/api/articles', checkAuth, checkAcl.user, require('./api/articles').get);
    app.get('/api/articles/:id', checkAuth, require('./api/articles').get);
    app.put('/api/articles', checkAuth, checkAcl.admin, require('./api/articles').put);
    app.delete('/api/articles', checkAuth, checkAcl.admin, require('./api/articles').delete);
    app.delete('/api/articles/:id', checkAuth, checkAcl.admin, require('./api/articles').delete);

    app.post('/api/register', multipartMiddleware, require('./api/register').post);




    app.use(function(req, res, next){
        res.status(404);
        log.debug('Not found URL: %s',req.url);
        res.send({ error: 'Not found' });
        return;
    });

    app.use(function(err, req, res, next){
        res.status(err.status || 500);
        log.error('Internal error(%d): %s',res.statusCode,err.message);
        res.send({ error: err.message });
        return;
    });

};
