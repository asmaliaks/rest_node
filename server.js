'use strict';
var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var log = require('./libs/log')(module);
var config = require('./libs/config');
var busboy = require('connect-busboy');
var app = express();

app.engine('ejs', require('ejs-locals'));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');


//app.use(favicon()); // favicon
app.use(busboy({ immediate: true })); // for file uploading
app.use(logger('dev')); // all requests and statuses in console
app.use(bodyParser.urlencoded({ extended: true })); // standart module for parsing JSON
app.use(bodyParser.json());
app.use(methodOverride()); // support PUT and DELETE
app.use(express.static(path.join(__dirname, "public"))); // launch static file server which watches into /public

require('./routes')(app);



app.listen(config.get('port'), function(){
  log.info('Express server listening on port ' + config.get('port'));
});